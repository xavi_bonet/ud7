package Ejercicios;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Scanner;

public class Ejercicio4App {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		//ArrayList que contiene la info de los productos en el carrito
		ArrayList<String[]> carrito = new ArrayList<String[]>();
		
		//Hashtable que contiene la info de los productos en stock
		Hashtable<String, String[]> stock = new Hashtable<String, String[]>();
		
		//Datos
		stock.put("Auricular", new String[] {"15", "156.93", "21"});
		stock.put("Portatil", new String[] {"5", "1789.99", "21"});
		stock.put("Ordenador", new String[] {"5", "2199.99", "21"});
		stock.put("Raton", new String[] {"25", "56.46", "21"});
		stock.put("Teclado", new String[] {"25", "63.99", "21"});
		stock.put("Pantalla", new String[] {"15", "199.99", "21"});
		stock.put("Movil", new String[] {"10", "299.99", "21"});
		stock.put("Camara", new String[] {"15", "71.32", "21"});
		stock.put("Altavoz", new String[] {"15", "65.99", "21"});
		stock.put("Alfombrilla", new String[] {"40", "13.99", "21"});
		
		//Menu PRINCIPAL
		char opcionMenu = ' ';
		do{
			//Opciones
			System.out.println("--- GESTION STOCK Y VENTAS --- ");
			System.out.println("");
			System.out.println("Que quieres hacer?");
			System.out.println("Stock: (1)");
			System.out.println("Carrito: (2)");
			System.out.println("Salir: (3)");
			
			opcionMenu = sc.nextLine().charAt(0);
			switch (opcionMenu) {
		      	
				//Menu STOCK
		    	case '1':
		    		
		    		char opcionMenu1 = ' ';
		    		do{
		    			//Opciones
		    			System.out.println("--- OPCIONES STOCK --- ");
		    			System.out.println("");
		    			System.out.println("Que quieres hacer?");
		    			System.out.println("A�adir productos (nuevo): (1)");
		    			System.out.println("A�adir articulos (editar): (2)");
		    			System.out.println("Consultar info de un producto: (3)");
		    			System.out.println("Listar info de todos los productos: (4)");
		    			System.out.println("Volver al menu principal: (5)");

		    			opcionMenu1 = sc.nextLine().charAt(0);
		    			switch (opcionMenu1) {
		    		      	
		    				//A�adir productos (nuevo)
		    		    	case '1':
		    		    		guardarDatosStock(stock);
		    		    		break;
		    		    	
		    		    	//A�adir articulos (editar)
		    		    	case '2':
		    		    		editarArticulosProducto(stock);
		    		    		break;
		    		    	
		    		    	//Consultar info de un producto
		    		    	case '3':
		    		    		consultarInfoProducto(stock);
		    		    		break;
		    		    	
		    		    	//Listar info de todos los productos
		    		    	case '4':
		    		    		mostrarDatosStock(stock);
		    		    		break;
		    		  	      
		    		    	default:
		    		    		break;
		    			}
		    		}while(opcionMenu1!='5');

		    		break;
		    	
		    	//Menu Carrito
		    	case '2':
		    		
		    		char opcionMenu2 = ' ';
		    		do{
		    			System.out.println("");
		    			System.out.println("Quieres a�adir mas articulos al carrito?");
		    			System.out.println("SI: (1)");
		    			System.out.println("NO: (2)");
		    			System.out.println("Volver al menu principal: (3)");

		    			opcionMenu2 = sc.nextLine().charAt(0);
		    			switch (opcionMenu2) {
		    		      	
		    				//A�adir articulos al carrito
		    		    	case '1':
		    		    		a�adirProductosCarrito(carrito, stock);
		    		    		break;
		    		    	
		    		    	//Mostar info y Pago
		    		    	case '2':
		    		    		realizarPago(carrito);
		    		    		break;

		    		    	default:
		    		    		break;
		    			}
		    		}while(opcionMenu2!='3');	

		    		break;

		    	default:
		    		break;
			}
		}while(opcionMenu!='3');

	}

	
	
	//Metodos
	
	
	//A�adir productos al carrito
	public static void a�adirProductosCarrito (ArrayList carrito, Hashtable <String, String[]> stock) {
		Scanner sc = new Scanner(System.in);
		
		//Pedir datos
		System.out.println("--- DATOS PRODUCTO --- ");
		System.out.println("");
		System.out.println("Introduce el nombre del producto: ");
		String nombreProducto = sc.nextLine();
		
		//Declarar enumeration para recorrer la clave y el valor
		Enumeration<String> clave = stock.keys();
		Enumeration<String[]> valor = stock.elements();
		
		int control = 0;
		
		//Recorrer para encontrar la clave y los datos
		while (clave.hasMoreElements() && valor.hasMoreElements()) { 
			String[] datos = new String[]{};
			datos = valor.nextElement();
			String productoBuscado = clave.nextElement();
			
			//Si se encuentra el producto buscado lo a�adimos al carrito
			if (productoBuscado.toLowerCase().equals(nombreProducto.toLowerCase())) {
				System.out.println("Nombre producto: " + productoBuscado + " | Numero de articulos: " + datos[0] + " | Precio del articulo: " + datos[1] + " | IVA del articulo: " + datos[2]);
				System.out.println("");
				System.out.println("Introduce el numero de articulos: ");
				int numArticulosPedidos = Integer.parseInt(sc.nextLine());
				int numArticulosDisponibles = Integer.parseInt(datos[0]);
				
				//Comprovar que no se a�aden mas articulos de los disponibles
				if (numArticulosPedidos <= numArticulosDisponibles) {
					
					//Quitar la cantidad correspondiente de articulos del stock
					int numArticulosActuales = numArticulosDisponibles - numArticulosPedidos;
					datos[0] = Integer.toString(numArticulosActuales);
					stock.put(productoBuscado, datos);
					
					//Guardar datos al carrito
					String numArticulosPedidosFinal = Integer.toString(numArticulosPedidos);
					String datosCarrito[] = {productoBuscado, numArticulosPedidosFinal, datos[1], datos[2]};
					carrito.add(datosCarrito);
				
				//Error
				} else {
					System.out.println("No hay stock suficiente");
				}
				
				control = 1;
			}
			
		}
		
		//Control de errores en la busqueda
		if (control == 0) {
			System.out.println("");
			System.out.println("El producto buscado no se ha encontrado");
			System.out.println("");
		}
	}
	
	
	
	//Realizar el pago
	public static void realizarPago (ArrayList carrito) {
		Scanner sc = new Scanner(System.in);
		
		//Si el carrito no esta vacio seguir con el pago
		if (!carrito.isEmpty()) {
		
		DecimalFormat formato1 = new DecimalFormat("#.##");
		
		System.out.println("--- DATOS CARRITO ---");
		System.out.println("");
		
		Iterator <String[]> iterator = carrito.iterator();
		String[] item;
		double totalSinIva = 0;
		double totalConIva = 0;
		
		while (iterator.hasNext()) {
			item = iterator.next();
			int cantidad = Integer.parseInt(item[1]);
			int iva = Integer.parseInt(item[3]);
			double noIva = Double.parseDouble(item[2]) * cantidad;
			double cantidadIva = noIva / 100 * iva;
			double siIva = noIva + cantidadIva;
			totalSinIva = totalSinIva + noIva;
			totalConIva = totalConIva + siIva;
			System.out.println("Producto: " + item[0] + " | Cantidad: " + cantidad + " | Precio total sin IVA: " + formato1.format(noIva) + " | Precio total con IVA: " + formato1.format(siIva) + " | IVA aplicado: " + iva + "%");
		}
		
		System.out.println("");
		System.out.println("--- DATOS PAGO ---");
		System.out.println("");
		System.out.println("Total sin IVA: " + formato1.format(totalSinIva));
		System.out.println("Total con IVA: " + formato1.format(totalConIva));
		System.out.println("");
		System.out.println("Introduce la cantidad que vas a pagar:");
		double cantidadPagada = Double.parseDouble(sc.nextLine());
		System.out.println("Canvio: " + formato1.format((cantidadPagada-totalConIva)));
		
		//Limpiar el carrito
		carrito.clear();
		
		//Si el carrito esta vacio mostrar "No hay nada en el carrito"
		} else {
			System.out.println("");
			System.out.println("No hay nada en el carrito");
			System.out.println("");
		}
		
	}
	
	
	
	//Guardar datos en el stock
	public static void guardarDatosStock(Hashtable <String, String[]> stock) {
		Scanner sc = new Scanner(System.in);
		
		//Obtener valores
		System.out.println(" ");
		System.out.println("--- DATOS NUEVO PRODUCTO STOCK --- ");
		System.out.println("");
		System.out.println("Introduce el nombre del producto: ");
		String nombreProducto = sc.nextLine();
		System.out.println("Introduce el numero de articulos: ");
		String numArticulos = sc.nextLine();
		System.out.println("Introduce el precio del articulo: ");
		String precioArticulo = sc.nextLine();
		System.out.println("Introduce el IVA del producto (4% o 21%): ");
		String ivaArticulo = sc.nextLine();
		
		//insertar valores
		stock.put(nombreProducto, new String[] {numArticulos, precioArticulo, ivaArticulo});
		
		//Mostrar valores insertados
		System.out.println("");
		System.out.println("El siguiente producto ha sido a�adido:");
		System.out.println("Nombre producto: " + nombreProducto + " | Numero de articulos: " + numArticulos + " | Precio del articulo: " + precioArticulo + " | IVA del articulo: " + ivaArticulo);
		System.out.println(" ");
	}
	
	
	
	//Mostrar datos guardados en el stock
	public static void mostrarDatosStock(Hashtable <String, String[]> stock) {
		System.out.println(" ");
		System.out.println("--- DATOS STOCK --- ");
		System.out.println("");
		
		//Declarar enumeration para recorrer la clave i el valor
		Enumeration<String> clave = stock.keys();
		Enumeration<String[]> valor = stock.elements();
		
		//Recorrer para mostrar los valores
		while (clave.hasMoreElements() && valor.hasMoreElements()) { 
			String[] datos = new String[]{};
			datos = valor.nextElement();
			System.out.println("Nombre producto: " + clave.nextElement() + " | Numero de articulos: " + datos[0] + " | Precio del articulo: " + datos[1] + " | IVA del articulo: " + datos[2]);  
		}
		System.out.println("");
	}
	
	
	
	//Editar los articulos de un producto stock
	public static void editarArticulosProducto(Hashtable <String, String[]> stock) {
		Scanner sc = new Scanner(System.in);
		
		//Obtener valores
		System.out.println(" ");
		System.out.println("--- EDITAR ARTICULOS PRODUCTO STOCK --- ");
		System.out.println("");
		System.out.println("Introduce el nombre del producto al que le quieres modificar el numero de articulos: ");
		String nombreProducto = sc.nextLine();

		
		//Declarar enumeration para recorrer la clave i el valor
		Enumeration<String> clave = stock.keys();
		Enumeration<String[]> valor = stock.elements();
		
		int control = 0;
		
		//Recorrer para encontrar la clave y modificar
		while (clave.hasMoreElements() && valor.hasMoreElements()) { 
			String[] datos = new String[]{};
			datos = valor.nextElement();
			String productoBuscado = clave.nextElement();
			
			//Si se enquentra el producto buscado pedimos el nuevo valor y lo modificamos
			if (productoBuscado.toLowerCase().equals(nombreProducto.toLowerCase())) {
				System.out.println("Introduce el nuevo numero de articulos: ");
				String numArticulos = sc.nextLine();
				datos[0] = numArticulos;
				stock.put(productoBuscado, datos);
				System.out.println("");
				System.out.println("El producto [ " + productoBuscado + " ] ahora cuenta con [ " + datos[0] + " ] articulos ");  
				System.out.println("");
				control = 1;
			} 
			
		}
		
		//Control de errores en la busqueda
		if (control == 0) {
			System.out.println("");
			System.out.println("El producto buscado no se ha encontrado");
			System.out.println("");
		}	
		
	}
	
	
	
	//Consultar info de un producto stock
	public static void consultarInfoProducto(Hashtable <String, String[]> stock) {
		Scanner sc = new Scanner(System.in);
		
		//Pedir valores
		System.out.println(" ");
		System.out.println("--- BUSCAR PRODUCTO STOCK --- ");
		System.out.println("");
		System.out.println("Introduce el nombre del producto: ");
		String nombreProducto = sc.nextLine();

		//Declarar enumeration para recorrer la clave y el valor
		Enumeration<String> clave = stock.keys();
		Enumeration<String[]> valor = stock.elements();
		
		int control = 0;
		
		//Recorrer para encontrar la clave y mostrar
		while (clave.hasMoreElements() && valor.hasMoreElements()) { 
			String[] datos = new String[]{};
			datos = valor.nextElement();
			String productoBuscado = clave.nextElement();
			
			//Si se encuentra el producto buscado mostramos
			if (productoBuscado.toLowerCase().equals(nombreProducto.toLowerCase())) {
				System.out.println("Nombre producto: " + productoBuscado + " | Numero de articulos: " + datos[0] + " | Precio del articulo: " + datos[1] + " | IVA del articulo: " + datos[2]);
				System.out.println("");
				control = 1;
			}
			
		}
		
		//Control de errores en la busqueda
		if (control == 0) {
			System.out.println("");
			System.out.println("El producto buscado no se ha encontrado");
			System.out.println("");
		}	
		
	}
	
}
