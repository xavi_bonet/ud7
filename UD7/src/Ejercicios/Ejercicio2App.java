package Ejercicios;

import java.util.Scanner;

import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

public class Ejercicio2App {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		//ArrayList que contiene la info de los productos
		ArrayList<String[]> carrito = new ArrayList<String[]>();
		
		//Menu
		char opcionMenu = ' ';
		do{
			System.out.println("");
			System.out.println("Quieres a�adir mas articulos al carrito?");
			System.out.println("SI: (1)");
			System.out.println("NO: (2)");
			System.out.println("");

			opcionMenu = sc.nextLine().charAt(0);
			switch (opcionMenu) {
		      	
				//A�adir articulos al carrito
		    	case '1':
		    		guardarDatos(carrito);
		    		break;

		    	default:
		    		break;
			}
		}while(opcionMenu!='2');
		
		//Mostar info y Pago
		realizarPago(carrito);

	}
	
	public static void guardarDatos (ArrayList carrito) {
		Scanner sc = new Scanner(System.in);
		System.out.println("--- DATOS PRODUCTO --- ");
		System.out.println("");
		System.out.println("Introduce el nombre del producto: ");
		String producto = sc.nextLine();
		System.out.println("Introduce el numero de articulos: ");
		String numArticulos = sc.nextLine();
		System.out.println("Introduce el precio del articulo: ");
		String precioArticulo = sc.nextLine();
		System.out.println("Introduce el IVA del producto (4% o 21%): ");
		String ivaArticulo = sc.nextLine();
		String datos[] = {producto, numArticulos, precioArticulo, ivaArticulo};
		carrito.add(datos);
	}
	
	public static void realizarPago (ArrayList carrito) {
		Scanner sc = new Scanner(System.in);
		DecimalFormat formato1 = new DecimalFormat("#.##");
		
		System.out.println("--- DATOS CARRITO ---");
		System.out.println("");
		Iterator <String[]> iterator = carrito.iterator();
		String[] item;
		double totalSinIva = 0;
		double totalConIva = 0;
		while (iterator.hasNext()) {
			item = iterator.next();
			int cantidad = Integer.parseInt(item[1]);
			int iva = Integer.parseInt(item[3]);
			double noIva = Double.parseDouble(item[2]) * cantidad;
			double cantidadIva = noIva / 100 * iva;
			double siIva = noIva + cantidadIva;
			totalSinIva = totalSinIva + noIva;
			totalConIva = totalConIva + siIva;
			System.out.println("Producto: " + item[0] + " | Cantidad: " + cantidad + " | Precio total sin IVA: " + formato1.format(noIva) + " | Precio total con IVA: " + formato1.format(siIva) + " | IVA aplicado: " + iva + "%");
		}
		System.out.println("");
		System.out.println("--- DATOS PAGO ---");
		System.out.println("");
		System.out.println("Total sin IVA: " + formato1.format(totalSinIva));
		System.out.println("Total con IVA: " + formato1.format(totalConIva));
		System.out.println("");
		System.out.println("Introduce la cantidad que vas a pagar:");
		double cantidadPagada = Double.parseDouble(sc.nextLine());
		System.out.println("Canvio: " + formato1.format((cantidadPagada-totalConIva)));
	}
}


