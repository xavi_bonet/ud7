package Ejercicios;

import java.util.Scanner;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Enumeration;

public class Ejercicio1App {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		DecimalFormat formato1 = new DecimalFormat("#.##");
		
		//Declarar dicionario
		Hashtable<Integer, String> alumnosNotas = new Hashtable<Integer, String>();
		
		//Menu
		char opcionMenu = ' ';
		do{
			System.out.println("Que quieres hacer?");
			System.out.println("Insertar alumno i notas: (1)");
			System.out.println("Listar alumnos: (2)");
			System.out.println("Salir: (3)");

			opcionMenu = sc.nextLine().charAt(0);
			switch (opcionMenu) {
		      	
				//Insertar alumnos i notas
		    	case '1':
		    		String nombre = obtenerNombreAlumno();
		    		double notaMedia = obtenerNotaMedia();
		    		String datos = "Nombre: " + nombre + " | Nota: " + formato1.format(notaMedia);
		    		int clave = alumnosNotas.size();
		    		alumnosNotas.put(clave, datos);
		    		break;
		        
		    	//Listar alumnos
		    	case '2': 
		    		//Declarar enumeration para recorrer el diccionario
		    		Enumeration<String> enumeration = alumnosNotas.elements();
		    		//Recorrer enumeration para mostrar los valores
		    		while (enumeration.hasMoreElements()) { 
		                System.out.println(enumeration.nextElement()); 
		            } 
		    		break;
	      
		    	default:
		    		break;
			}
		}while(opcionMenu!='3');
	}
	
	//Obtener nombre del alumno
	public static String obtenerNombreAlumno() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce el nombre del alumno: ");
		String nombre = sc.nextLine();
		return nombre;
	}
	
	//Obtener notas i calcular nota media
	public static double obtenerNotaMedia() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Quantas notas quieres introducir: ");
		int totalNotas = Integer.parseInt(sc.nextLine());
		double sumaNotas = 0;
		for (int i = 1; i <= totalNotas; i++) {
			System.out.println("Introduce la nota " + i + ": ");
			sumaNotas = Double.parseDouble(sc.nextLine()) + sumaNotas;
		}
		double notaMedia = sumaNotas / totalNotas;
		return notaMedia;
	}
	
}