package Ejercicios;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Scanner;

public class Ejercicio3App {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		//Declarar diccionario "stock"
		Hashtable<String, String[]> stock = new Hashtable<String, String[]>();
		
		/**
		 * Diccionario
		 * clave				datos
		 * nombreProducto	  	numArticulos, precioArticulo, ivaArticulo
		 **/
		
		//Datos
		stock.put("Auricular", new String[] {"15", "156.93", "21"});
		stock.put("Portatil", new String[] {"5", "1789.99", "21"});
		stock.put("Ordenador", new String[] {"5", "2199.99", "21"});
		stock.put("Raton", new String[] {"25", "56.46", "21"});
		stock.put("Teclado", new String[] {"25", "63.99", "21"});
		stock.put("Pantalla", new String[] {"15", "199.99", "21"});
		stock.put("Movil", new String[] {"10", "299.99", "21"});
		stock.put("Camara", new String[] {"15", "71.32", "21"});
		stock.put("Altavoz", new String[] {"15", "65.99", "21"});
		stock.put("Alfombrilla", new String[] {"40", "13.99", "21"});
		
		//Menu STOCK
		char opcionMenu = ' ';
		do{
			//Opciones
			System.out.println("--- OPCIONES STOCK --- ");
			System.out.println("");
			System.out.println("Que quieres hacer?");
			System.out.println("A�adir productos (nuevo): (1)");
			System.out.println("A�adir articulos (editar): (2)");
			System.out.println("Consultar info de un producto: (3)");
			System.out.println("Listar info de todos los productos: (4)");
			System.out.println("Salir: (5)");

			opcionMenu = sc.nextLine().charAt(0);
			switch (opcionMenu) {
		      	
				//A�adir productos (nuevo)
		    	case '1':
		    		guardarDatosStock(stock);
		    		break;
		    	
		    	//A�adir articulos (editar)
		    	case '2':
		    		editarArticulosProducto(stock);
		    		break;
		    	
		    	//Consultar info de un producto
		    	case '3':
		    		consultarInfoProducto(stock);
		    		break;
		    	
		    	//Listar info de todos los productos
		    	case '4':
		    		mostrarDatosStock(stock);
		    		break;
		  	      
		    	default:
		    		break;
			}
		}while(opcionMenu!='5');
		
	}
	
	//Guardar datos en el stock
	public static void guardarDatosStock(Hashtable <String, String[]> stock) {
		Scanner sc = new Scanner(System.in);
		
		//Obtener valores
		System.out.println(" ");
		System.out.println("--- DATOS NUEVO PRODUCTO STOCK --- ");
		System.out.println("");
		System.out.println("Introduce el nombre del producto: ");
		String nombreProducto = sc.nextLine();
		System.out.println("Introduce el numero de articulos: ");
		String numArticulos = sc.nextLine();
		System.out.println("Introduce el precio del articulo: ");
		String precioArticulo = sc.nextLine();
		System.out.println("Introduce el IVA del producto (4% o 21%): ");
		String ivaArticulo = sc.nextLine();
		
		//insertar valores
		stock.put(nombreProducto, new String[] {numArticulos, precioArticulo, ivaArticulo});
		
		//Mostrar valores insertados
		System.out.println("");
		System.out.println("El siguiente producto ha sido a�adido:");
		System.out.println("Nombre producto: " + nombreProducto + " | Numero de articulos: " + numArticulos + " | Precio del articulo: " + precioArticulo + " | IVA del articulo: " + ivaArticulo);
		System.out.println(" ");
	}
	
	
	//Mostrar datos guardados en el stock
	public static void mostrarDatosStock(Hashtable <String, String[]> stock) {
		System.out.println(" ");
		System.out.println("--- DATOS STOCK --- ");
		System.out.println("");
		
		//Declarar enumeration para recorrer la clave i el valor
		Enumeration<String> clave = stock.keys();
		Enumeration<String[]> valor = stock.elements();
		
		//Recorrer para mostrar los valores
		while (clave.hasMoreElements() && valor.hasMoreElements()) { 
			String[] datos = new String[]{};
			datos = valor.nextElement();
			System.out.println("Nombre producto: " + clave.nextElement() + " | Numero de articulos: " + datos[0] + " | Precio del articulo: " + datos[1] + " | IVA del articulo: " + datos[2]);  
		}
		System.out.println("");
	}
	
	
	//Editar los articulos de un producto
	public static void editarArticulosProducto(Hashtable <String, String[]> stock) {
		Scanner sc = new Scanner(System.in);
		
		//Obtener valores
		System.out.println(" ");
		System.out.println("--- EDITAR ARTICULOS PRODUCTO STOCK --- ");
		System.out.println("");
		System.out.println("Introduce el nombre del producto al que le quieres modificar el numero de articulos: ");
		String nombreProducto = sc.nextLine();

		
		//Declarar enumeration para recorrer la clave i el valor
		Enumeration<String> clave = stock.keys();
		Enumeration<String[]> valor = stock.elements();
		
		int control = 0;
		
		//Recorrer para encontrar la clave y modificar
		while (clave.hasMoreElements() && valor.hasMoreElements()) { 
			String[] datos = new String[]{};
			datos = valor.nextElement();
			String productoBuscado = clave.nextElement();
			
			//Si se enquentra el producto buscado pedimos el nuevo valor y lo modificamos
			if (productoBuscado.toLowerCase().equals(nombreProducto.toLowerCase())) {
				System.out.println("Introduce el nuevo numero de articulos: ");
				String numArticulos = sc.nextLine();
				datos[0] = numArticulos;
				stock.put(productoBuscado, datos);
				System.out.println("");
				System.out.println("El producto [ " + productoBuscado + " ] ahora cuenta con [ " + datos[0] + " ] articulos ");  
				System.out.println("");
				control = 1;
			} 
			
		}
		
		//Control de errores en la busqueda
		if (control == 0) {
			System.out.println("");
			System.out.println("El producto buscado no se ha encontrado");
			System.out.println("");
		}	
		
	}
	
	
	//Consultar info de un producto
	public static void consultarInfoProducto(Hashtable <String, String[]> stock) {
		Scanner sc = new Scanner(System.in);
		
		//Pedir valores
		System.out.println(" ");
		System.out.println("--- BUSCAR PRODUCTO STOCK --- ");
		System.out.println("");
		System.out.println("Introduce el nombre del producto: ");
		String nombreProducto = sc.nextLine();

		//Declarar enumeration para recorrer la clave y el valor
		Enumeration<String> clave = stock.keys();
		Enumeration<String[]> valor = stock.elements();
		
		int control = 0;
		
		//Recorrer para encontrar la clave y mostrar
		while (clave.hasMoreElements() && valor.hasMoreElements()) { 
			String[] datos = new String[]{};
			datos = valor.nextElement();
			String productoBuscado = clave.nextElement();
			
			//Si se encuentra el producto buscado mostramos
			if (productoBuscado.toLowerCase().equals(nombreProducto.toLowerCase())) {
				System.out.println("Nombre producto: " + productoBuscado + " | Numero de articulos: " + datos[0] + " | Precio del articulo: " + datos[1] + " | IVA del articulo: " + datos[2]);
				System.out.println("");
				control = 1;
			}
			
		}
		
		//Control de errores en la busqueda
		if (control == 0) {
			System.out.println("");
			System.out.println("El producto buscado no se ha encontrado");
			System.out.println("");
		}	
		
	}
	
}



